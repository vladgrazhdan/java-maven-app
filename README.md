# Jenkins CI/CD pipeline for Java app project

## Intro

Jenkins CI/CD pipeline with automatic release version increment.

## Overview

*   [x] **Can be modified and extended**
*   [x] **Tested**

### Requirements

* Jenkins v2.333+
* Docker

### Install

Use git to clone this repository locally.

### Usage

Jenkins pipeline config files from this repository.
