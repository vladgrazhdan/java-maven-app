def buildJar() {
    echo 'building the app...'
    sh 'mvn package'
}

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t repo/my-repo:jma-2.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push repo/my-repo:jma-2.0'
    }
}

def deployApp(){
    echo "Building the application..."
                    sh 'mvn package'
}

return this
